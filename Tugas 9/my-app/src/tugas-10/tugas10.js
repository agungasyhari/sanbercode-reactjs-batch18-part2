import React from 'react';


class Name extends React.Component{
render() {
        return <p>{this.props.nama}</p>;
        }   
      }

class Price extends React.Component {
    render () {
        return <p>{this.props.harga}</p>
    }
}

class Weight extends React.Component {
    render () {
        return <p>{this.props.berat}</p>
    }
}

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500},
]; 

class Tugas10 extends React.Component {
    render() {
        return (
            <>
                    <table style={{border:"3px solid black", margin: "0 auto", marginTop: "20px", width: "300px"}}>
                        <thead>
                            <tr id="row0">
                                <td style={{border:"1px solid black"}}>Nama</td>
                                <td style={{border:"1px solid black"}}>Harga</td>
                                <td style={{border:"1px solid black"}}>Berat</td>
                            </tr>
                        </thead>
                        <tbody>
                        {dataHargaBuah.map(el=>{
                            return (
                            <tr id="row1">
                            <td style={{border:"1px solid black"}}> <Name nama={el.nama} /> </td>
                            <td style={{border:"1px solid black"}}> <Price harga={el.harga} /></td>
                            <td style={{border:"1px solid black"}}> <Weight berat={el.berat} /></td>
                            </tr>
                            )
                        })}
                        </tbody>
                    </table>
            </>
        )
    }
}
export default Tugas10


